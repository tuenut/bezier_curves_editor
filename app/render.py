import pygame

from typing import List

from app.state import AppState
from utils.types import ABCBezierCurve, ABCBezierCurvesBunch


white = pygame.Color(255, 255, 255)
gray = pygame.Color(100, 100, 100)
lightgray = pygame.Color(200, 200, 200)
red = pygame.Color(255, 0, 0)
green = pygame.Color(0, 255, 0)
blue = pygame.Color(0, 0, 255)
black = pygame.Color(0, 0, 0)


class AppRender:
    app_state: AppState

    def __init__(self, state: AppState):
        self.screen = pygame.display.set_mode((1024, 768))
        self.font = pygame.font.SysFont('mono', 12, bold=True)
        self.app_state = state

    def update(self, curves_bunch: List[ABCBezierCurvesBunch], text):
        ### Draw stuff
        self.screen.fill(white)

        for bunch in curves_bunch:
            self._draw(bunch)

        self.screen.blit(
            self.font.render(self.app_state.app_mode, True, (255, 255, 255)),
            pygame.Vector2(10, 10)
        )

        # self.screen.blit(
        #     self.font.render(str(self.app_state.selected_point), True, (255, 255, 255)),
        #     pygame.Vector2(10, 30)
        # )
        if isinstance(text, list):
            for line_number in range(len(text)):
                line = text[line_number]
                self.__draw_text(line, offset=line_number)
        elif isinstance(text, str):
            self.__draw_text(text)

        ### Flip screen
        pygame.display.flip()

    def __draw_text(self, text: str, offset=0):
        self.screen.blit(
            self.font.render(text, True, (255, 255, 255)),
            pygame.Vector2(10, 60 + 10 * offset)
        )

    def _draw(self, curve_bunch: ABCBezierCurvesBunch):
        for curve in curve_bunch.curves:
            self._draw_curve(curve)

    def _draw_curve(self, curve: ABCBezierCurve):
        ### Draw control points
        for p in curve.vertices:
            if p.is_selected:
                pygame.draw.circle(self.screen, green, (p.x, p.y), 10)
            pygame.draw.circle(self.screen, blue, (int(p.x), int(p.y)), 4)

        if len(curve.vertices) == 4 and curve.points:
            ### Draw control "lines"
            pygame.draw.lines(self.screen, lightgray, False, curve.vertices)
            ### Draw bezier curve
            pygame.draw.lines(self.screen, black, False, curve.points, 2)
