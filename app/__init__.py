import json
import pygame

from app.events import EventManager
from app.events.subscriptions import LMB, RMB
from app.bezier import BezierCurve, BezierCurvesBunch
from app.render import AppRender
from app.state import AppState


class DataManagement:
    @property
    def data(self):
        data = {"curves": []}

        for curve in self._curves:
            curve_data = {
                "vertices": [(v.x, v.y) for v in curve.vertices]
            }
            data["curves"].append(curve_data)

        return data

    def save(self, event: pygame.event.Event):
        with open("trek.json", "w") as file:
            json.dump(self.data, file, indent=4)

    def open(self, event: pygame.event.Event):
        raise NotImplementedError("Should be rewritten for BezierCurvesBunch")


class App:
    FPS = 100

    events: EventManager
    state: AppState

    def __init__(self):
        self.events = EventManager()
        self.state = AppState()

        pygame.init()
        self.clock = pygame.time.Clock()

        self.render = AppRender(self.state)

        self.__subscribe_events()

    @property
    def help_text(self):
        return self.state.app_mode_display

    def run(self):
        self.state.is_run = True

        self.__main_loop()

    def __main_loop(self):
        while self.state.is_run:
            self.events.handle_events()
            self.__update_stuff()
            self.__render_stuff()
            self.clock.tick(self.FPS)

    def __update_stuff(self):
        self.state.update()

    def __render_stuff(self):
        self.render.update(self.state.curves, self.help_text)

    def _exit(self, event: pygame.event.Event):
        self.state.is_run = False

    def __subscribe_events(self):
        self.events.subscribe(
            on_event=pygame.QUIT,
            callback=self._exit
        )
        self.events.subscribe(
            on_key_down=pygame.K_q,
            callback=self._exit,
        )

        self.events.subscribe(
            on_key_down=pygame.K_a,
            callback=self.state.add_curve
        )
        # self.events.subscribe(
        #     on_key_down=pygame.K_s,
        #     callback=self.save
        # )
        # self.events.subscribe(
        #     on_key_down=pygame.K_o,
        #     callback=self.open
        # )
