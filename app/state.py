from __future__ import annotations

from typing import List, Optional

import pygame

from loguru import logger

from app.bezier import BezierCurvesBunch, BezierCurveVertex
from app.events import KeysController, dispatch_quit_event
from utils.decorators import as_singleton


class PointsManagement:
    def _select_point(self: AppState, event: pygame.event.Event):
        pos = pygame.Vector2(event.pos)

        for curve_bunch in self.curves:
            try:
                point = curve_bunch.get_vertex(pos)
            except curve_bunch.VertexNotFound:
                continue
            else:
                point.select()
                self.selected_point = point
                self._set_events_for_moving_point()
                return

    def _set_events_for_moving_point(self: AppState):
        self.keys_control.set_lmb_function(self._save_point_position)
        self.keys_control.set_rmb_function(self._cancel_point_selection)
        self.keys_control.set_escape_function(self._cancel_point_selection)

    def _save_point_position(self: AppState, event: pygame.event.Event):
        self.selected_point.save()
        self.selected_point = None

        self._set_normal_mode()

    def _cancel_point_selection(self: AppState, event: pygame.event.Event):
        self.selected_point.reset()
        self.selected_point = None

        self._set_normal_mode()


class CurvesManagement:
    def add_curve(self: AppState, event: pygame.event.Event):
        """Command to eneter in 'Create curve mode'."""

        self._set_creating_mode()
        self._temp_curve = BezierCurvesBunch()

    def _add_point_to_temp_curve(self: AppState, event: pygame.event.Event):
        logger.debug(f"<{event.pos}> <{type(event.pos)}>")
        self._temp_curve.add_vertex(event.pos)

        if len(self._temp_curve.vertices) % 4 == 0:
            self._set_curve_completion_mode()

    def _cancel_create_curve(self: AppState, event: pygame.event.Event):
        del self._temp_curve
        self._set_normal_mode()

    def _complete_curve(self: AppState, event: pygame.event.Event):
        logger.debug("complete curve call")
        self._curves_bunches.append(self._temp_curve)
        self._set_normal_mode()


@as_singleton
class AppState(PointsManagement, CurvesManagement):
    _curves_bunches: List[BezierCurvesBunch]
    _temp_curve: Optional[BezierCurvesBunch] = None
    selected_point: Optional[BezierCurveVertex] = None

    __app_mode = None
    __ran = False

    MODE_CURVE_CREATING = "creating_curve"
    MODE_CURVE_COMPLETION = "curve_completion"
    MODE_NORMAL = "normal"

    MODE_TEXTS = {
        MODE_CURVE_COMPLETION: "Press ENTER to save curve",
        MODE_CURVE_CREATING: "Left click to add new point",
        MODE_NORMAL: "Press A to add new curve, or LMB to select point for moving"
    }

    keys_control: KeysController

    @property
    def app_mode(self) -> str:
        return self.__app_mode

    @property
    def is_run(self) -> bool:
        return self.__ran

    @is_run.setter
    def is_run(self, value: bool):
        if isinstance(value, bool):
            self.__ran = value
        else:
            raise TypeError("run state can be True or False!")

    @property
    def curves(self) -> List[BezierCurvesBunch]:
        if self._temp_curve:
            return [*self._curves_bunches, self._temp_curve]
        else:
            return [*self._curves_bunches]

    def __init__(self):
        self.keys_control = KeysController()
        self._curves_bunches = []

        self._set_normal_mode()

    def update(self):
        for curve_bunch in self._curves_bunches:
            curve_bunch.update()

        if self._temp_curve:
            self._temp_curve.update()

    def app_mode_display(self) -> str:
        return self.MODE_TEXTS.get(self.app_mode, self.app_mode)

    def _set_creating_mode(self):
        self.__app_mode = self.MODE_CURVE_CREATING

        self.keys_control.set_lmb_function(self._add_point_to_temp_curve)
        self.keys_control.set_escape_function(self._cancel_create_curve)

    def _set_curve_completion_mode(self):
        self.__app_mode = self.MODE_CURVE_COMPLETION

        self.keys_control.set_enter_function(self._complete_curve)

    def _set_normal_mode(self):
        logger.debug("Set normal mode")

        self._temp_curve = None
        self.__app_mode = self.MODE_NORMAL

        self.keys_control.set_lmb_function(self._select_point)
        self.keys_control.unset_rmb()
        self.keys_control.set_escape_function(dispatch_quit_event)
        self.keys_control.unset_enter()
