"""
The code from [[here | https://www.drdobbs.com/forward-difference-calculation-of-bezier/184403417?pgno=5]]
 is taken as a basis for working with bezier curves
"""

from __future__ import annotations

import pygame

from typing import Union, List, Tuple
from loguru import logger

from utils.types import ABCBezierCurve, ABCBezierCurvesBunch, ABCBezierCurveVertex


class BezierCurvesBunch(ABCBezierCurvesBunch):
    """List abstraction of curves bunch.

    To draw a few sequence curves, where the last point previous curve should
     be the first point of next curve. That class manages that.
    """

    curves: List[BezierCurve]

    def __init__(self):
        self.curves = [BezierCurve(), ]
        self.vertices = []

        self.__selected_curve = None
        self.__selected_point = None

        logger.debug(f"Create new curves bunch <{self}>")

    def add_vertex(self, vector: Tuple[float | int, float | int]):
        curve = self.curves[-1]

        try:
            curve_vertex = curve.create_vertex(vector)
        except curve.NoMorePoints:
            curve = self.__create_next_curve()
            curve_vertex = curve.create_vertex(vector)

        self.vertices.append(curve_vertex)

    def get_vertex(self, position: pygame.Vector2) -> BezierCurveVertex:
        for curve in self.curves:
            try:
                return curve.get_point(position)
            except curve.VertexNotFound:
                continue

        raise self.VertexNotFound(f"Curves bunch have no vertex there "
                                  f"<{position}>.")

    def update(self):
        for curve in self.curves:
            curve.update()

    def __create_next_curve(self) -> BezierCurve:
        logger.debug(f"Add new curve in <{self}>.")

        curve = BezierCurve()
        self.curves.append(curve)

        previous_curve = self.curves[-2]
        last_point = previous_curve.vertices[-1]
        curve.add_vertex(last_point)

        return curve

    def __repr__(self):
        return f"<{self.__class__.__name__}> with {len(self.vertices)}"


class BezierCurve(ABCBezierCurve):
    __curve_resolution: int
    __step_size: float
    __changed: bool = False

    def __init__(self, curve_resolution: int = 6):
        self.vertices = []
        self.points: List[pygame.Vector2] = []
        self._set_resolution(curve_resolution)

    def create_vertex(self, position: Tuple[int | tuple, int | tuple])\
            -> BezierCurveVertex:
        curve_vertex = BezierCurveVertex(position, curve=self)
        self.add_vertex(curve_vertex)

        return curve_vertex

    def add_vertex(self, vertex: BezierCurveVertex, create_new=True):
        if not isinstance(vertex, BezierCurveVertex):
            raise TypeError("Point should be an instance of BezierCurveVertex.")

        if len(self.vertices) == 4:
            raise self.NoMorePoints("Curve can have only 4 points. You cant add more.")

        self.vertices.append(vertex)
        self.__changed = True

    def get_point(self, position: pygame.Vector2):
        for point in self.vertices:
            if abs(point.x - position.x) < 5 and abs(point.y - position.y) < 5:
                return point

        raise self.VertexNotFound(f"Curve have no vertex there <{position}>.")

    def update(self):
        if len(self.vertices) != 4:
            return

        if self.__changed or self.is_point_selection:
            self.__recalculate()

        for point in self.vertices:
            point.update()

        self.__changed = False

    @classmethod
    def load(cls, vertices: list):
        curve = cls()
        curve.vertices = [BezierCurveVertex(v, curve=curve) for v in vertices]

    @property
    def is_point_selection(self) -> bool:
        return any(point.is_selected for point in self.vertices)

    def __recalculate(self):
        self.points = []

        point, first_FD, second_FD, third_FD = self.__get_forward_differences()

        # Compute points at each step
        self.points.append(pygame.Vector2(point.x, point.y))

        for i in range(self.__curve_resolution):
            point.x += first_FD.x
            point.y += first_FD.y

            first_FD.x += second_FD.x
            first_FD.y += second_FD.y

            second_FD.x += third_FD.x
            second_FD.y += third_FD.y

            self.points.append(pygame.Vector2(point))

    def __get_polynomial_coefs(self):
        # Compute polynomial coefficients from Bezier points
        ax = -self.vertices[0].x + 3 * self.vertices[1].x + -3 * self.vertices[2].x + self.vertices[3].x
        ay = -self.vertices[0].y + 3 * self.vertices[1].y + -3 * self.vertices[2].y + self.vertices[3].y

        bx = 3 * self.vertices[0].x + -6 * self.vertices[1].x + 3 * self.vertices[2].x
        by = 3 * self.vertices[0].y + -6 * self.vertices[1].y + 3 * self.vertices[2].y

        cx = -3 * self.vertices[0].x + 3 * self.vertices[1].x
        cy = -3 * self.vertices[0].y + 3 * self.vertices[1].y

        dx = self.vertices[0].x
        dy = self.vertices[0].y

        return pygame.Vector2(ax, ay), pygame.Vector2(bx, by), \
               pygame.Vector2(cx, cy), pygame.Vector2(dx, dy)

    def __get_forward_differences(self):
        a, b, c, d = self.__get_polynomial_coefs()

        # Compute forward differences from Bezier points and "h"
        pointX = d.x
        pointY = d.y

        firstFDX = a.x * (self._step_size ** 3) + b.x * (self._step_size ** 2) + c.x * self._step_size
        firstFDY = a.y * (self._step_size ** 3) + b.y * (self._step_size ** 2) + c.y * self._step_size

        secondFDX = 6 * a.x * (self._step_size ** 3) + 2 * b.x * (self._step_size ** 2)
        secondFDY = 6 * a.y * (self._step_size ** 3) + 2 * b.y * (self._step_size ** 2)

        thirdFDX = 6 * a.x * (self._step_size ** 3)
        thirdFDY = 6 * a.y * (self._step_size ** 3)

        return pygame.Vector2(pointX, pointY), pygame.Vector2(firstFDX, firstFDY), \
               pygame.Vector2(secondFDX, secondFDY), pygame.Vector2(thirdFDX, thirdFDY)

    @property
    def _step_size(self):
        return self.__step_size

    def _set_resolution(self, value):
        self.__curve_resolution = value
        self.__step_size = 1.0 / self.__curve_resolution

        self.__changed = True

    def __repr__(self):
        return f"<{self.__class__.__name__}> {str(self.vertices)} at <{id(self)}>"


class BezierCurveVertex(ABCBezierCurveVertex):
    __selected: bool = False
    __old_position: pygame.Vector2 | None = None
    curve: BezierCurve

    def __init__(self, *args, curve: BezierCurve = None):
        if curve is None:
            raise TypeError
        super().__init__(*args)
        self.curve = curve

    def select(self):
        if not self.is_selected:
            self.__selected = True
            self.__old_position = pygame.Vector2(self)

    def save(self):
        self.__selected = False
        self.__old_position = None

    def reset(self):
        self.__selected = False
        self.update(self.__old_position)

    @property
    def is_selected(self) -> bool:
        return self.__selected

    def update(self, *args, **kwargs):
        if args or kwargs:
            super().update(*args, **kwargs)

        if self.is_selected:
            super().update(pygame.mouse.get_pos())
