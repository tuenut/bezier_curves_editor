import sys

from app import App

from loguru import logger

from utils.constants import WARN_ONLY_LOGGING


logger.remove()


def warn_only_filter(record: dict):
    if record["extra"].get(WARN_ONLY_LOGGING):
        return record["level"].no == logger.level("WARNING").no
    return True


logger.add(sys.stderr, filter=warn_only_filter)

if __name__ == '__main__':
    app = App()
    app.run()
