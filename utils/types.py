from __future__ import annotations

import pygame

from typing import Union, Tuple, List, Optional, Callable

from abc import ABC, abstractmethod


class _BezierCurveInterface(ABC):
    @abstractmethod
    def update(self):
        ...

    @abstractmethod
    def add_vertex(self, vector: Union[pygame.Vector2, Tuple[float, float]]) \
            -> ABCBezierCurveVertex:
        ...


class ABCBezierCurve(_BezierCurveInterface, ABC):
    __curve_resolution: int
    __step_size: float

    vertices: List[ABCBezierCurveVertex]
    points: List[pygame.Vector2]

    class NoMorePoints(Exception):
        pass

    class VertexNotFound(Exception):
        pass

    @property
    @abstractmethod
    def _step_size(self) -> float:
        ...

    @abstractmethod
    def _set_resolution(self, value):
        ...


class ABCBezierCurvesBunch(_BezierCurveInterface, ABC):
    curves: List[ABCBezierCurve]
    vertices: List[ABCBezierCurveVertex]

    class VertexNotFound(Exception):
        pass


class ABCEvenManager(ABC):
    @abstractmethod
    def subscribe(
            self,
            callback: Callable,
            on_key_down: Optional[int] = None,
            on_mouse_button: Optional[int] = None,
            kwargs: Optional[list] = None,
            as_args: bool = False
    ) -> str:
        ...

    @abstractmethod
    def unsubscribe(self, subscription_id: str):
        ...

    @abstractmethod
    def check_events(self):
        ...


class ABCBezierCurveVertex(pygame.Vector2, ABC):
    @abstractmethod
    def __init__(self, *args, curve: ABCBezierCurve = None):
        super().__init__(*args)

    @abstractmethod
    def select(self):
        ...

    @abstractmethod
    def save(self):
        ...

    @abstractmethod
    def reset(self):
        ...

    @property
    @abstractmethod
    def is_selected(self):
        ...
